.PHONY: figure
figure: output/figures/heatmap.png

output/data/maptest.csv : src/test_data.R
	@echo --- Generating test data ---
	@mkdir -p $(@D)
	./$<
output/data/data_in.Rds : src/model_input.R output/data/maptest.csv
	@echo --- Prepping input ---
	@mkdir -p $(@D)
	./$<

output/data/samples.Rds : src/runmodel.R src/model.stan output/data/maptest.csv output/data/data_in.Rds
	@echo --- Running model ---
	@mkdir -p $(@D)
	./$<
output/figures/heatmap.png : src/hotspot_sim.R output/data/samples.Rds output/data/data_in.Rds
	@echo --- Making figure ---
	@mkdir -p $(@D)
	./$<

.PHONY: clean
clean:
	rm -r output
