data {
  int N; //Number of points
  int G; //Number of hotspots
  matrix[N,2] coords; //X,Y coordinates of points

  real x_lim[2];
  real y_lim[2];


}

transformed data {
  real area;

  area <- (x_lim[2]-x_lim[1])*(y_lim[2]-y_lim[1]);

}

parameters {

  //X and Y positions of hotspot centers
  vector<lower=x_lim[1],upper=x_lim[2]>[G] hotspot_x;
  vector<lower=y_lim[1],upper=y_lim[2]>[G] hotspot_y;

  real<lower=0> sigma_mu;
  vector<lower=0>[G] sigma; //hotspot 'bandwidth'
  real<lower=0> lambda; //total hazard from hotspots
  real<lower=0> gamma; //minimum background hazard

  ## Parameters for stick-breaking prior
  vector<lower=0, upper=1>[G] beta_incr; //Stick-breaking increments
  real<lower=0> alpha; //Concentration parameter
}

transformed parameters {

  vector[G] beta; //DP mixture probabilities

  ## Translate IID samples from beta distribution to mixture probabilities
  beta[1] <- beta_incr[1];
  for(i in 2:G) {
    real escape_p;
    escape_p <- 1;
    for (j in 1:(i-1)) {
      escape_p <- escape_p*(1-beta_incr[j]);
    }
    beta[i] <- beta_incr[i]*escape_p;
  }

  beta <- beta/sum(beta);

}

model {
  vector[N] pt_density;

  hotspot_x ~ normal(0,1);
  hotspot_y ~ normal(0,1);
  beta_incr ~ beta(1, alpha);
  alpha ~ cauchy(0, 1);
  gamma ~ normal(0, 10);
  sigma ~ normal(sigma_mu, 0.01);

  ## First do contribution of background rate 
  for (i in 1:N) {
    pt_density[i] <- gamma/area;
  }
  increment_log_prob(-gamma);

  ## Contribution of each hotspot to risk at each point
  for (i in 1:N) {
    for (j in 1:G) {
      pt_density[i] <- pt_density[i]+ lambda*beta[j]*exp(normal_log(coords[i,1], hotspot_x[j], sigma[j])+normal_log(coords[i,2], hotspot_y[j], sigma[j]));
    }
  }

  increment_log_prob(sum(log(pt_density)));

  ## Survival for each hotspot
  for (i in 1:G) {
    real x_dens;
    real y_dens;
  
    x_dens <- normal_cdf(x_lim[2], hotspot_x[i], sigma[i]) - normal_cdf(x_lim[1], hotspot_x[i], sigma[i]);
    y_dens <- normal_cdf(y_lim[2], hotspot_y[i], sigma[i]) - normal_cdf(y_lim[1], hotspot_y[i], sigma[i]);
    
    increment_log_prob(-lambda*beta[i]*x_dens*y_dens);
  }
  
}

generated quantities {

  real total_exposure;
  int total_cases;

  total_exposure <- gamma;
  total_cases <- poisson_rng(alpha);
  
  for (i in 1:G) {

    real x_dens;
    real y_dens;
  
    x_dens <- normal_cdf(x_lim[2], hotspot_x[i], sigma[i]) - normal_cdf(x_lim[1], hotspot_x[i], sigma[i]);
    y_dens <- normal_cdf(y_lim[2], hotspot_y[i], sigma[i]) - normal_cdf(y_lim[1], hotspot_y[i], sigma[i]);

    total_cases <- total_cases + poisson_rng(lambda*beta[i]*x_dens*y_dens);
    
    total_exposure <- total_exposure + lambda*beta[i]*x_dens*y_dens;
  }

}
